import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLiteMain {
    
	public static void main(String[] args) {
		final String url="klienci.db";
		System.out.println("��czenie z baz� danych "+url+"...");
        try {
			Connection con=DriverManager.getConnection("jdbc:sqlite:"+url);
			System.out.println("OK");
			
			try(Statement st=con.createStatement()){
				
				DatabaseMetaData dbmd=con.getMetaData();
				ResultSet rsTables= dbmd.getTables(null, null, null, null);
				ResultSet cols;
				int[] fieldWidth;
				while(rsTables.next()){
					String tabName=rsTables.getString("TABLE_NAME");
					System.out.println(tabName);
					ResultSet rsTab= st.executeQuery("select * from "+tabName);
					ResultSetMetaData rsmd1=rsTab.getMetaData();
					int colCount=rsmd1.getColumnCount();
					fieldWidth=new int[colCount];
					for(int i=1;i<=colCount;i++){
						String type=rsmd1.getColumnTypeName(i);
						int colWidth=0;
						//sqlite nie podaje poprawnych rozmiar�w p�l, dlatego dla int ustawiamy arbitralnie 10 a dla pozosta�ych 25
						if(type.equals("INTEGER"))
							fieldWidth[i-1]=10;
						else
							fieldWidth[i-1]=25;
						System.out.printf("%-"+Integer.toString(fieldWidth[i-1])+"s", rsmd1.getColumnName(i));	
					}
					System.out.println("");
					while(rsTab.next()){
						for(int i=1;i<=rsmd1.getColumnCount();i++)
							System.out.printf("%-"+Integer.toString(fieldWidth[i-1])+"s", rsTab.getString(i));
						System.out.println("");	
					}
				}
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
